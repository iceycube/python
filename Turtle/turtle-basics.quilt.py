import turtle
from turtle import *
turtle.Screen().bgcolor("black")
def part( total, length, breadth, col ):
    angleInc = 360/total
    width( breadth )
    color( col )
    for i in range(total):
        forward( length )
        left( angleInc )
def rosette( total, length, width, color, angleInc ):
  for i in range( int(360/angleInc) ):
        part( total, length, width, color )
        left( angleInc )

turtle.setup( 500, 500, 500, 500 ) 
turtle.speed(50) 


title("turtle-basics.py")
rosette(10,40,1,"red",10)
rosette(5,80,1,"white",10)
turtle.exitonclick()



